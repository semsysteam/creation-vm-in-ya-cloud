#!/usr/bin/env python3

import argparse
import logging
from atlassian import Confluence


def main():
    logging.basicConfig(level=logging.WARNING)
    arguments = parse_args()

    base_url = arguments.url

    new_content = '<p>Стенд на базе перерываемой ВМ от Яндекса, при простое 24 часа будет остановлена, данные будут сохранены. При следующем запуске будет другой IP-адрес</p><p><a href="http://{0}:15672">http://{0}:15672</a> rabbitmq</p><p><a href="http://{0}:5444">http://{0}:5444</a>  pgadmin</p><p><a href="http://{0}:5432">http://{0}:5432</a> СУБД Postgre</p><p><a href="http://{0}:5015/hf">http://{0}:5015/hf</a> notifier</p><p><a href="http://{0}:5013">http://{0}:5013</a> poiadm</p><p><a href="http://{0}:5012/swagger/">http://{0}:5012/swagger/</a> webapi</p><p><a href="http://{0}:5011/swagger/">http://{0}:5011/swagger/</a> isapi</p><p><a href="http://{0}:5010/dashboard/">http://{0}:5010/dashboard/</a> is</p><p><a href="http://{0}:5009">http://{0}:5009</a> crm</p><p><a href="http://{0}:5008">http://{0}:5008</a> id</p><p><a href="http://{0}:3000">http://{0}:3000</a> rocketchat</p><p />'.format(arguments.vm_ipv4_address)
    
    try:
        confluence = Confluence(
            url=base_url, username=arguments.username,
            password=arguments.token, cloud=True)
        api_version = confluence.api_version
        space = arguments.space
        title = arguments.title
        if api_version:
            # Check page exists
            page_exists = confluence.page_exists(
                space=space, title=title)
            if page_exists:
                # Provide content id from search result by title and space
                page_id = confluence.get_page_id(space, title)
                if page_id:
                    # Update page body by ID
                    confluence.update_page( page_id=page_id, title=title, body=new_content,)                   
            confluence.close
        else:
            raise Exception('Unable to connect to Atlassian Confluence')

    except Exception as exc:
        logging.error(exc.with_traceback)
        raise


def parse_args():
    try:
        parser = argparse.ArgumentParser(
            description=__doc__,
            formatter_class=argparse.RawTextHelpFormatter)
        auth = parser.add_mutually_exclusive_group(required=True)
        auth.add_argument(
            '--token', help='Confluence user API token',)
        parser.add_argument(
            '--username', help='Confluence user name', required=True,)
        parser.add_argument(
            '--url', help='Your Confluence url', required=True,)
        parser.add_argument(
            '--space', help='Your Confluence space', required=True,)
        parser.add_argument(
            '--title', help='Your Confluence space', required=True,)
        parser.add_argument(
            '--vm_ipv4_address', help='Your public Yandex VM Instance IPv4 address', required=True,)
        user_args = parser.parse_args()
        return user_args
    except Exception as exc:
        logging.error(exc)


if __name__ == '__main__':
    main()
