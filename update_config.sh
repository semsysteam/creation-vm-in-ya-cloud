#!/usr/bin/env bash
set -eu
#
# Run with arguments
# ./update_config.sh -u <ADM_PATH Value> -a <API_PATH Value> -i <IS_PATH Value> -s <ISAPI_PATH Value> -n <NOTIFIER_PATH Value> -p <PGADMIN_PATH Value>
#
#

# shellcheck disable=SC1091
. .env

if [ -z "$*" ]; then
  echo "No options found!"
  exit 1
fi

while getopts ":u:a:i:s:n:p:" flag; do
  case "$flag" in
  u)
    VM_ADM_PATH=${OPTARG}
    ;;
  a)
    VM_API_PATH=${OPTARG}
    ;;
  i)
    VM_IS_PATH=${OPTARG}
    ;;
  s)
    VM_ISAPI_PATH=${OPTARG}
    ;;
  n)
    VM_NOTIFIER_PATH=${OPTARG}
    ;;
  p)
    VM_PGADMIN_PATH=${OPTARG}
    ;;
  :)
    echo "Option -$OPTARG requires an argument (getopts)" >&2
    exit 1
    ;;
  *)
    echo "No reasonable options found!"
    ;;
  esac
done

echo -e "[ * ] Создание конфигурационных файлов ..."

if [ -f "$VM_NOTIFIER_PATH/appsettings.$ASPNETCOREENV.json" ]; then
  sudo rm -f "$VM_NOTIFIER_PATH/appsettings.$ASPNETCOREENV.json"
fi

# Создаем файл конфигурации для notifier
cat <<EOF >"$VM_NOTIFIER_PATH/appsettings.$ASPNETCOREENV.json"
{
  "ConnectionStrings": {
    "NotifierDb": "Host=$PG_DEFAULT_HOST; Port=$PG_DEFAULT_PORT; User Id=$PG_OWNER_USER; Password=$PG_OWNER_PASSWORD; Database=$PG_DEFAULT_DB;"
  },
  "ApiAddress": "http://webapi/",
  "Identity": {
    "Login": "$API_USER_NAME",
    "Password": "$API_USER_PASSWORD"
  },
  "SystemNotificationChannelsOntologySysName": "SYSTEM_NOTIFICATION_CHANNELS_ONTOLOGY",
  "SystemNotificationProvidersOntologySysName": "SYSTEM_NOTIFICATION_PROVIDERS_ONTOLOGY",
  "RabbitMQ": {
    "Host": "$RABBITMQ_HOST",
    "UserName": "$RABBITMQ_USER",
    "Password": "$RABBITMQ_PASSWORD",
    "RetryCount": "5",
    "SubscriptionClientName": "$NOTIFIER_MQ_CLIENT_NAME",
    "BrokerName": "$RABBITMQ_BROKER_NAME"
  },
  "Logging": {
    "LogLevel": {
      "Default": "Warning",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Warning",
      "POI.Notifier": "Warning"
    }
  }
}
EOF

if [ -f "${VM_PGADMIN_PATH}/servers.json" ]; then
  sudo rm -f "${VM_PGADMIN_PATH}/servers.json"
fi

# Создаем файл конфигурации для PgAdmin
cat <<EOF >"${VM_PGADMIN_PATH}/servers.json"
{
  "Servers": {
    "Postgres": {
      "Name": "PostgreSQL",
      "Port": "$PG_DEFAULT_PORT",
      "Group": "Servers",
      "SSLMode": "prefer",
      "Username": "$PG_OWNER_USER",
      "Host": "$PG_DEFAULT_HOST",
      "MaintenanceDB": "$PG_DEFAULT_DB"
    }
  }
}
EOF

if [ -f "$VM_API_PATH/appsettings.json" ]; then
  sudo rm -f "$VM_API_PATH/appsettings.json"
fi

# Создаем файл конфигурации для WebAPI
cat <<EOF >"$VM_API_PATH/appsettings.json"
{
  "Logging": {
    "IncludeScopes": false,
    "LogLevel": {
      "Default": "Warning",
      "Matrix.LogicalProvider": "Warning",
      "Matrix.WebAPI": "Warning",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Warning"
    }
  },
  "AllowedHosts": "*"
}
EOF

if [ -f "$VM_API_PATH/appsettings.$ASPNETCOREENV.json" ]; then
  sudo rm -f "$VM_API_PATH/appsettings.$ASPNETCOREENV.json"
fi

# Создаем файл конфигурации для WebAPI
cat <<EOF >"$VM_API_PATH/appsettings.$ASPNETCOREENV.json"
{
  "ConnectionStrings": {
    "B-Matrix": {
      "ConnectionString": "Host=$PG_DEFAULT_HOST; Port=$PG_DEFAULT_PORT; User Id=$PG_OWNER_USER; Password=$PG_OWNER_PASSWORD; Database=$PG_DEFAULT_DB; Pooling=True; Minimum Pool Size=0; Maximum Pool Size=200; Application Name=poi-api;",
      "ProviderName": "Matrix.Storage.PostgreSQL.Provider"
    }
  },
  "AppSettings": {
    "SkipPrincipalCheck": false,
    "DefaultConnection": "B-Matrix",
    "LDAPName": "",
    "LDAPContainer": "",
    "TokenLife": 12,
    "IdentityServerAuthority": "http://is/",
    "IdentityServerAudience": "$IS_CLIENT_AUDIENCE",
    "IdentityServerApi": "http://isapi/",
    "IdentityServerClientId": "$IS_CLIENT_ID",
    "IdentityServerClientSecret": "$IS_CLIENT_SECRET",
    "IdentityServerClientScope": "$IS_CLIENT_SCOPE",
    "PermissionFile": "$PERMISSON_FILE",
    "TemplatingSystemWebApi": "$T_SYSTEM_WEB_API",
    "TemplatingSystemBpDescriptionDocTemplateGuid": "5c655151-a9a3-44bb-97fa-18360432a242",
    "TemplatingSystemJobDescriptionDocTemplateGuid": "99553c2d-8d11-49f0-95ce-8b3b6381cdd7",
    "ApiKey": "3F30554C-93FC-4DC0-A9D8-19CE44A4A421"
  },
  "ChangeLogSettings": {
    "Db": {
      "DefaultSchema": "logs",
      "Provider": "PostgreSQL"
    },
    "RabbitMQ": {
      "Host": "$RABBITMQ_HOST",
      "UserName": "$RABBITMQ_USER",
      "Password": "$RABBITMQ_PASSWORD",
      "RetryCount": "5",
      "SubscriptionClientName": "$API_MQ_CLIENT_NAME",
      "BrokerName": "$RABBITMQ_BROKER_NAME"
    }
  },
  "Logging": {
    "IncludeScopes": false,
    "LogLevel": {
      "Default": "Warning",
      "Matrix.WebAPI": "Warning",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Warning"
    }
  }
}
EOF

if [ -f "$VM_ISAPI_PATH/appsettings.json" ]; then
  sudo rm -f "$VM_ISAPI_PATH/appsettings.json"
fi

# Создаем файл конфигурации для API идентификации
cat <<EOF >"$VM_ISAPI_PATH/appsettings.json"
{
  "ConnectionStrings": {
    "UserDb": null
  },
  "EmailSender": {
    "Host": "$SMTP_ADDRESS",
    "Port": "$SMTP_PORT",
    "EnableSSL": "$SMTP_ENABLE_SSL",
    "UserName": "$SMTP_USERNAME",
    "Password": "$SMTP_PASSWORD"
  },
  "Logging": {
    "LogLevel": {
      "Default": "Warning"
    },
    "Console": {
      "LogLevel": {
        "Default": "Warning"
      }
    }
  },
  "AllowedHosts": "*"
}
EOF

if [ -f "$VM_ISAPI_PATH/appsettings.$ASPNETCOREENV.json" ]; then
  sudo rm -f "$VM_ISAPI_PATH/appsettings.$ASPNETCOREENV.json"
fi

# Создаем файл конфигурации для API идентификации в текущей среде
cat <<EOF >"$VM_ISAPI_PATH/appsettings.$ASPNETCOREENV.json"
{
  "ConnectionStrings": {
    "UserDb": "host=$PG_DEFAULT_HOST;port=$PG_DEFAULT_PORT;database=users_db;user id=$PG_OWNER_USER;Password=$PG_OWNER_PASSWORD;Command Timeout=0"
  },
  "EmailVerificationSettings": {
    "BaseUrl": "http://$EXTERNAL_SERVER_ADDRESS:$ID_PORT",
    "UserVerificationAddress": "verifications",
    "UserResetAddress": "reset_password_frm",
    "EmailAddress": "$EMAIL_SENDER"
  },
  "DefaultInitUser": {
    "Email": "admin@admin.com",
    "Password": "Vg38jfrfwq#"
  },
  "AuthSettings": {
    "ClientId": "$IS_CLIENT_ID",
    "ClientSecret": "$IS_CLIENT_SECRET",
    "ClientScope": "$IS_CLIENT_SCOPE",
    "IdentityUrl": "http://is/"
  },
  "ApiAudience": "$IS_CLIENT_AUDIENCE",
  "Logging": {
    "LogLevel": {
      "Default": "Warning",
      "System": "Warning",
      "Microsoft": "Warning"
    }
  }
}
EOF

if [ -f "$VM_IS_PATH/appsettings.$ASPNETCOREENV.json" ]; then
  sudo rm -f "$VM_IS_PATH/appsettings.$ASPNETCOREENV.json"
fi

# Создаем файл конфигурации для сервера идентификации в текущей среде
cat <<EOF >"$VM_IS_PATH/appsettings.$ASPNETCOREENV.json"
{
  "ConnectionStrings": {
    "IdentityDb": "host=$PG_DEFAULT_HOST;port=$PG_DEFAULT_PORT;database=identity_db;user id=$PG_OWNER_USER;Password=$PG_OWNER_PASSWORD;",
    "UserDb": "host=$PG_DEFAULT_HOST;port=$PG_DEFAULT_PORT;database=users_db;user id=$PG_OWNER_USER;Password=$PG_OWNER_PASSWORD;"
  },
  "EmailVerificationSettings": {
    "BaseUrl": "http://$EXTERNAL_SERVER_ADDRESS:$ID_PORT",
    "UserVerificationAddress": "verifications",
    "UserResetAddress": "reset_password_frm",
    "EmailAddress": "$EMAIL_SENDER"
  },
  "DefaultInitUser": {
    "Email": "admin@admin.com",
    "Password": "Vg38jfrfwq#"
  },
  "AuthSettings": {
    "ClientId": "$IS_CLIENT_ID",
    "ClientSecret": "$IS_CLIENT_SECRET",
    "ClientScope": "$IS_CLIENT_SCOPE",
    "IdentityUrl": "http://is/"
  },
  "ApiAudience": "$IS_CLIENT_AUDIENCE",
  "ApiAddress": "http://isapi/",
  "Logging": {
    "LogLevel": {
      "Default": "Warning",
      "System": "Warning",
      "Microsoft": "Warning"
    }
  }
}
EOF

if [ -f "$VM_IS_PATH/appsettings.json" ]; then
  sudo rm -f "$VM_IS_PATH/appsettings.json"
fi

# Создаем файл конфигурации для сервера идентификации
cat <<EOF >"$VM_IS_PATH/appsettings.json"
{
  "ConnectionStrings": {
    "IdentityDb": null,
    "UserDb": null
  },
  "Logging": {
    "LogLevel": {
      "Default": "Warning"
    },
    "Console": {
      "LogLevel": {
        "Default": "Warning"
      }
    }
  },
  "AllowedHosts": "*"
}
EOF

if [ -f "$VM_ADM_PATH/api.js" ]; then
  sudo rm -f "$VM_ADM_PATH/api.js"
fi

# Создаем файл конфигурации для интерфейса администратора
cat <<EOF >"$VM_ADM_PATH/api.js"
var g_base_url = "http://$EXTERNAL_SERVER_ADDRESS:$API_PORT";
var g_isapi_base_url = "http://$EXTERNAL_SERVER_ADDRESS:$ISAPI_PORT";
EOF

sleep 1
echo -e "[ ✔ ] Создание конфигурационных файлов ➜ ЗАВЕРШЕНО!\n"
