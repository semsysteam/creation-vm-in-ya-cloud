#!/usr/bin/env bash
set -eu
#
# Скрипт установки зависимостей для использования pg_dump и pg_restore
#

PG_VER=13

# Проверка сущестования команды
command_exists() {
    command -v "$@" >/dev/null 2>&1
}

if ! command_exists python3; then
    sudo apt-get update -qq
    sudo apt-get -yqq install python3
fi
if ! command_exists pip3; then
    sudo apt-get update -qq
    sudo apt-get -yqq install python3-pip
fi

if ! command_exists pg_dump || ! command_exists pg_restore; then
    sudo apt-get update -qq && sudo apt-get install --no-install-recommends postgresql-client-common postgresql-client-"$PG_VER" -y -qq
fi
