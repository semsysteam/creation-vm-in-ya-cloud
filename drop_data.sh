#!/usr/bin/env bash
set -eu
#
# Скрипт удаления данных
#

# shellcheck disable=SC1091
. .env

# Проверка сущестования команды
command_exists() {
   command -v "$@" >/dev/null 2>&1
}

if command_exists psql; then
   sudo -u postgres psql --username postgres --no-password --set ON_ERROR_STOP=on <<EOF
\x
DROP DATABASE IF EXISTS $PG_DEFAULT_DB;
EOF

   psql_exit_status=$?
   if [ $psql_exit_status != 0 ]; then
      echo -e "[ X ] ОШИБКА: Выполнение sql запроса закончилось с ошибкой ${psql_exit_status}.\n" 1>&2
      exit $psql_exit_status
   else
      echo -e "[ ✔ ] Удаление БД ➜ ЗАВЕРШЕНО!\n"
   fi
else
   cat >&2 <<-'EOF'

		ВНИМАНИЕ: утилита psql не установлена в вашей системе.
    
		EOF
    exit 1
fi
