#!/usr/bin/env bash
set -eu
#
# Скрипт удаления данных
#

# shellcheck disable=SC1091
. .env

# Проверка сущестования команды
command_exists() {
    command -v "$@" >/dev/null 2>&1
}

if command_exists docker; then
    echo -e "[ * ] Создание базы данных сервисов ..."

    docker pull "${DOCKER_REGISTRY}"/dev-core/db-creator:latest
    docker pull "${DOCKER_REGISTRY}"/dev-core/filler:latest

    sleep 1s
    docker run --rm "$DOCKER_REGISTRY"/dev-core/db-creator:latest -sh "$PG_DEFAULT_HOST" -sp "$PG_DEFAULT_PORT" -du "$PG_DBA_USER" -dp "$PG_DBA_PASSWORD" -ou "$PG_OWNER_USER" -op "$PG_OWNER_PASSWORD" -dbn "$PG_DEFAULT_DB"
    sleep 1s
    echo -e "[ ✔ ] Создание базы данных сервисов ➜  ЗАВЕРШЕНО!\n"
    echo -e "[ * ] Заполнение базы данных сервисов ..."
    docker run --rm "$DOCKER_REGISTRY"/dev-core/filler:latest -cs "Host=$PG_DEFAULT_HOST; Port=$PG_DEFAULT_PORT; User Id=$PG_OWNER_USER; Password=$PG_OWNER_PASSWORD; Database=$PG_DEFAULT_DB;"
    sleep 1s
    echo -e "[ ✔ ] Заполнение базы данных сервисов ➜  ЗАВЕРШЕНО!\n"
else
    cat >&2 <<-'EOF'

		ВНИМАНИЕ: утилита docker не установлена в вашей системе.
    
		EOF
    exit 1
fi
