#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import datetime
import gzip
import logging
import os
import subprocess
import tempfile
from shutil import move
from tempfile import mkstemp
from typing import Any, List

import boto3
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

BUCKET_PATH = '/'
BACKUP_PATH = '/tmp/'


class YCStorageManager(object):
    def __init__(self, secret_access_key=None, access_key_id=None,  endpoint_url=None, bucket_name=None, region=None):
        self.secret_access_key = secret_access_key
        self.access_key_id = access_key_id
        self.region_name = region
        self.endpoint_url = endpoint_url
        self.bucket_name = bucket_name
        self.backup_path = BACKUP_PATH
        self.s3_objects_keys = []

    def upload(self, file_full_path, dst_file):
        """
        Upload a file to an S3 bucket.
        """
        try:
            s3_client = boto3.client('s3', endpoint_url=self.endpoint_url, aws_access_key_id=self.access_key_id,
                                     aws_secret_access_key=self.secret_access_key, region_name=self.region_name)
            boto3.setup_default_session()
            s3_client.upload_file(
                file_full_path, self.bucket_name, BUCKET_PATH + dst_file)
            os.remove(file_full_path)
        except boto3.exceptions.S3UploadFailedError as exc:
            logger.error(exc)
            exit(1)

    def download(self, backup_s3_key, dst_file):
        """
        Upload a file to an S3 bucket.
        """
        try:
            s3_client = boto3.resource('s3', endpoint_url=self.endpoint_url, aws_access_key_id=self.access_key_id,
                                       aws_secret_access_key=self.secret_access_key, region_name=self.region_name)
            s3_client.meta.client.download_file(
                self.bucket_name, backup_s3_key, dst_file)
        except Exception as exc:
            logger.error(exc)
            exit(1)

    def delete(self, key):
        keys = [{'Key': key}]
        try:
            logger.info(
                "Delete object {} in S3 Storage...".format(key))
            s3_client = boto3.client('s3', endpoint_url=self.endpoint_url, aws_access_key_id=self.access_key_id,
                                     aws_secret_access_key=self.secret_access_key, region_name=self.region_name)
            boto3.setup_default_session()
            s3_client.delete_objects(
                Bucket=self.bucket_name, Delete={'Objects': keys})
            logger.info("Object {} deleted".format(key))
        except boto3.exceptions.S3UploadFailedError as exc:
            logger.error(exc)
            exit(1)

    def list_local_available(self) -> List:
        """
        List available db backups in local storage.
        """
        key_list = []
        dump_objects = os.listdir()
        for key in dump_objects:
            key_list.append(key)
        return key_list

    def list_remote_available(self) -> List:
        """
        List available db backups in S3 storage.
        """
        objects_keys = []
        s3_client = boto3.client('s3', endpoint_url=self.endpoint_url, aws_access_key_id=self.access_key_id,
                                 aws_secret_access_key=self.secret_access_key, region_name=self.region_name)
        boto3.setup_default_session()
        s3_objects = s3_client.list_objects_v2(
            Bucket=self.bucket_name, Prefix=BUCKET_PATH)
        logger.info(s3_objects)
        for key in s3_objects['Contents']:
            objects_keys.append(key['Key'])
        return objects_keys

    def list_postgres_databases(self, host, database_name, port, user, password) -> bytes:
        try:
            process = subprocess.Popen(
                ['psql',
                 '--dbname=postgresql://{}:{}@{}:{}/{}'.format(
                     user, password, host, port, database_name),
                 '--list'],
                stdout=subprocess.PIPE
            )
            output = process.communicate()[0]
            if int(process.returncode) != 0:
                logger.error('Command failed. Return code : {}'.format(
                    process.returncode))
                exit(1)
            return output
        except Exception as e:
            logger.error(e)
            exit(1)

    def compress_file(self, src_file) -> str:
        compressed_file = "{}.gz".format(str(src_file))
        with open(src_file, 'rb') as f_in:
            with gzip.open(compressed_file, 'wb') as f_out:
                for line in f_in:
                    f_out.write(line)
        return compressed_file

    def extract_file(self, src_file) -> Any:
        extracted_file, _ = os.path.splitext(src_file)
        with gzip.open(src_file, 'rb') as f_in:
            with open(extracted_file, 'wb') as f_out:
                for line in f_in:
                    f_out.write(line)
        return extracted_file

    def change_user_from_dump(self, source_dump_path, old_user, new_user):
        fh, abs_path = mkstemp()
        with os.fdopen(fh, 'w') as new_file:
            with open(source_dump_path) as old_file:
                for line in old_file:
                    new_file.write(line.replace(old_user, new_user))
        # Remove original file
        os.remove(source_dump_path)
        # Move new file
        move(abs_path, source_dump_path)

    def remove_faulty_statement_from_dump(self, src_file):
        temp_file, _ = tempfile.mkstemp()
        try:
            with open(temp_file, 'w+'):
                process = subprocess.Popen(
                    ['pg_restore',
                     '-l'
                     '-v',
                     src_file],
                    stdout=subprocess.PIPE
                )
                output = subprocess.check_output(
                    ('grep', '-v', '"EXTENSION - plpgsql"'), stdin=process.stdout)
                process.wait()
                if int(process.returncode) != 0:
                    logger.error('Command failed. Return code : {}'.format(
                        process.returncode))
                    exit(1)

                os.remove(src_file)
                with open(src_file, 'w+') as cleaned_dump:
                    subprocess.call(
                        ['pg_restore',
                         '-L'],
                        stdin=output,
                        stdout=cleaned_dump
                    )

        except Exception as exc:
            logger.error("Issue when modifying dump : {}".format(exc))
            print("Issue when modifying dump : {}".format(exc))

    def create_db(self, host, database_name, port, user, password):
        """
        Create postgres db.
        """
        try:
            con = psycopg2.connect(dbname='postgres', port=port,
                                   user=user, host=host,
                                   password=password)
        except Exception as exc:
            logger.error(exc)
            exit(1)
        con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = con.cursor()
        cur.execute(
            "DROP DATABASE IF EXISTS {} WITH (FORCE) ;".format(database_name))
        cur.execute(
            "CREATE DATABASE {} WITH OWNER {} ENCODING = 'UTF-8' ;".format(database_name, user))
        cur.execute("GRANT ALL PRIVILEGES ON DATABASE {} TO {} ;".format(
            database_name, user))
        return database_name

    def backup_postgres_db(self, host, database_name, port, user, password, dst_file, verbose) -> bytes:
        """
        Backup postgres db to a file.
        """
        prcss = None
        if verbose:
            prcss = ['pg_dump', '--dbname=postgresql://{}:{}@{}:{}/{}'.format(
                user, password, host, port, database_name),
                '--format=c', 
                '--exclude-schema=bi', '--exclude-schema=scheduler', '--exclude-schema=monitoring', '--exclude-schema=monitor',
                '--exclude-table=public.total_r',
                '--file={}'.format(dst_file),
                '--verbose']
        else:
            prcss = ['pg_dump', '--dbname=postgresql://{}:{}@{}:{}/{}'.format(
                user, password, host, port, database_name),
                '--format=custom',
                '--exclude-schema=bi', '--exclude-schema=scheduler', '--exclude-schema=monitoring', '--exclude-schema=monitor',
                '--exclude-table=public.total_r',
                '--file={}'.format(dst_file)]
        try:
            process = subprocess.Popen(prcss, stdout=subprocess.PIPE)
            output = process.communicate()[0]

            if process.returncode != 0:
                logger.error('Command failed. Return code : {}'.format(
                    process.returncode))
                exit(1)
            return output
        except Exception as exc:
            logger.error(exc)
            exit(1)

    def restore_postgres_db(self, host, database_name, port, user, password, src_file, verbose) -> bytes:
        """
        Restore postgres db from a file.
        """
        prcss = None
        if verbose:
            prcss = ['pg_restore',
                     '--no-owner',
                     '--clean',
                     '--exit-on-error',
                     '--format=c',
                     '--no-privileges',
                     '--if-exists',
                     '--no-data-for-failed-tables',
                     '--single-transaction',
                     '--dbname=postgresql://{}:{}@{}:{}/{}'.format(user,
                                                                   password,
                                                                   host,
                                                                   port, database_name),
                     '-v',
                     src_file]

        else:
            prcss = ['pg_restore',
                     '--no-owner',
                     '--clean',
                     '--exit-on-error',
                     '--format=c',
                     '--no-privileges',
                     '--if-exists',
                     '--no-data-for-failed-tables',
                     '--single-transaction',
                     '--dbname=postgresql://{}:{}@{}:{}/{}'.format(user,
                                                                   password,
                                                                   host,
                                                                   port, database_name),
                     src_file]
        try:
            process = subprocess.Popen(
                prcss,
                stdout=subprocess.PIPE
            )
            output = process.communicate()[0]
            if int(process.returncode) != 0:
                logger.error('Command failed. Return code : {}'.format(
                    process.returncode))
            return output
        except Exception as exc:
            logger.error("Issue with the db restore : {exc}")


def parse_args() -> Any:
    """
    Parse arguments scripts.
    """
    parser = argparse.ArgumentParser(
        description='S3 Storage management')
    auth = parser.add_mutually_exclusive_group(required=False)
    auth.add_argument(
        '--secret_key', metavar='secret_key', help='S3 Storage user secret accesskey')
    parser.add_argument("--verbose",
                        default=False,
                        help="verbose output")
    parser.add_argument("--access_key_id",
                        metavar="access_key_id",
                        default=None,
                        required=False,
                        help='S3 Storage user access key id')
    parser.add_argument("--endpoint_url", metavar="endpoint_url",
                        default=None,
                        required=False,
                        help="S3 Storage endpoint url")
    parser.add_argument("--region", metavar="region",
                        default=None,
                        required=False,
                        help="S3 Storage region name")
    parser.add_argument("--bucket_name", metavar="bucket_name",
                        default=None,
                        required=False,
                        help="S3 Storage bucket name")
    parser.add_argument("--action",
                        metavar="action",
                        choices=['list', 'list_dbs', 'restore', 'backup'],
                        required=True)
    parser.add_argument("--date",
                        metavar="YYYYMMdd",
                        default=None,
                        required=False,
                        help="Date to use for restore (show with --action list)")
    parser.add_argument("--pg_src_host",
                        metavar="pg_src_host",
                        default=None,
                        required=True,
                        help="Host of the source PG server")
    parser.add_argument("--pg_src_port",
                        metavar="pg_src_port",
                        default=None,
                        required=True,
                        help="Port of the source PG server")
    parser.add_argument("--pg_src_user",
                        metavar="pg_src_user",
                        default=None,
                        required=True,
                        help="User name of the PG server")
    parser.add_argument("--pg_src_password",
                        metavar="pg_password",
                        default=None,
                        required=True,
                        help="User password of the PG server")
    parser.add_argument("--pg_src_db",
                        metavar="pg_src_db",
                        default=None,
                        required=True,
                        help="Name of the source database")
    parser.add_argument("--pg_dst_host",
                        metavar="pg_dst_host",
                        default=None,
                        required=True,
                        help="Host of the destination PG server")
    parser.add_argument("--pg_dst_port",
                        metavar="pg_dst_port",
                        default=None,
                        required=True,
                        help="Port of the source PG server")
    parser.add_argument("--pg_dst_user",
                        metavar="pg_dst_user",
                        default=None,
                        required=True,
                        help="User name of the destination PG server")
    parser.add_argument("--pg_dst_password",
                        metavar="pg_dst_password",
                        default=None,
                        required=True,
                        help="User password of the destination PG server")
    parser.add_argument("--pg_dst_db",
                        metavar="pg_dst_db",
                        default=None,
                        required=True,
                        help="Name of the destination database")
    parser.add_argument("--delete_dump",
                        metavar="delete_dump",
                        default=False,
                        required=False,
                        help="Delete dumps")
    args = parser.parse_args()
    return args


def main():
    arguments = parse_args()
    timestr = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
    filename = 'backup-{}-{}.dump'.format(timestr, arguments.pg_src_db)
    filename_compressed = '{}.gz'.format(filename)
    tmp_filename = '/tmp/restore.dump.gz'
    tmp_uncompressed = '/tmp/restore.dump'
    local_file_path = '{}{}'.format(BACKUP_PATH, filename)

    logger.info("Listing S3 bucket s3://{}/{} content :".format(arguments.bucket_name,
                                                                BUCKET_PATH))
    ycsm = YCStorageManager(secret_access_key=arguments.secret_key,
                            access_key_id=arguments.access_key_id, endpoint_url=arguments.endpoint_url, bucket_name=arguments.bucket_name, region=arguments.region)
    # list task
    if arguments.action == "list":
        if arguments.secret_key is None or arguments.access_key_id is None or arguments.bucket_name is None or arguments.endpoint_url is None:
            ycsm.s3_objects_keys = ycsm.list_local_available()
            for key in ycsm.s3_objects_keys:
                logger.info("Key : {}".format(key))
        else:
            logger.info("Listing S3 bucket s3://{}/{} content :".format(arguments.bucket_name,
                                                                        BUCKET_PATH))
            ycsm.s3_objects_keys = ycsm.list_remote_available()
            for key in ycsm.s3_objects_keys:
                logger.info("Key : {}".format(key))
    # list databases task
    elif arguments.action == "list_dbs":
        result = ycsm.list_postgres_databases(host=arguments.pg_src_host, database_name=arguments.pg_src_db, port=arguments.pg_src_port,
                                              user=arguments.pg_src_user, password=arguments.pg_src_password)
        for line in result.splitlines():
            logger.info(line)
    # backup task
    elif arguments.action == "backup":
        logger.info("Backing up {} database to {}".format(
            arguments.pg_src_db, local_file_path))
        result = ycsm.backup_postgres_db(host=arguments.pg_src_host, database_name=arguments.pg_src_db, port=arguments.pg_src_port,
                                         user=arguments.pg_src_user, password=arguments.pg_src_password, dst_file=local_file_path, verbose=arguments.verbose)
        for line in result.splitlines():
            logger.info(line)
        logger.info("Backup complete")
        # compress dump file and ulpoad to S3
        if arguments.secret_key is not None and arguments.access_key_id is not None and arguments.bucket_name is not None and arguments.endpoint_url is not None:
            logger.info("Compressing {}".format(local_file_path))
            comp_file = ycsm.compress_file(local_file_path)
            if comp_file is not None:
                logger.info("Uploading {} to S3 Storage...".format(comp_file))
                ycsm.upload(comp_file, filename_compressed)
                logger.info("Uploaded to {}".format(filename_compressed))

    # restore task
    elif arguments.action == "restore":
        if not arguments.date:
            logger.warning('No date was chosen for restore. Run again with the "list" '
                           'action to see available restore dates')
        else:
            if os.path.exists(tmp_filename):
                try:
                    os.remove(tmp_filename)
                except Exception as exc:
                    logger.error(exc)

            all_backup_keys = None
            if arguments.secret_key is None or arguments.access_key_id is None or arguments.bucket_name is None or arguments.endpoint_url is None:
                all_backup_keys = ycsm.list_local_available()
            else:
                all_backup_keys = ycsm.list_remote_available()
            if all_backup_keys is None:
                logger.error(
                    "No match found for backups with date : {}".format(arguments.date))
                exit(1)
            else:
                backup_match = [
                    s for s in all_backup_keys if arguments.date in s]
                if backup_match:
                    logger.info(
                        "Found the following backup : {}".format(backup_match))
                else:
                    logger.error(
                        "No match found for backups with date : {}".format(arguments.date))
                    logger.info("Available keys : {}".format(
                        [s for s in all_backup_keys]))
                    exit(1)

                logger.info("Downloading {} from S3 into : {}".format(
                    backup_match[0], tmp_filename))
                ycsm.download(backup_match[0], tmp_filename)
                logger.info("Download complete")
                logger.info("Extracting {}".format(tmp_filename))
                ext_file = ycsm.extract_file(tmp_filename)
                logger.info("Extracted to : {}".format(ext_file))
                logger.info(
                    "Creating temp database for restore : {}".format(arguments.pg_dst_db))
                tmp_database = ycsm.create_db(host=arguments.pg_dst_host, database_name=arguments.pg_dst_db,
                                              port=arguments.pg_dst_port, user=arguments.pg_dst_user, password=arguments.pg_dst_password)
                logger.info(
                    "Created temp database for restore : {}".format(tmp_database))
                logger.info("Restore starting")
                result = ycsm.restore_postgres_db(host=arguments.pg_dst_host, database_name=arguments.pg_dst_db, port=arguments.pg_dst_port,
                                                  user=arguments.pg_dst_user, password=arguments.pg_dst_password, src_file=tmp_uncompressed,
                                                  verbose=arguments.verbose)
                for line in result.splitlines():
                    logger.info(line)
                logger.info("Database restored")

                if arguments.delete_dump:
                    if bool(arguments.delete_dump) is True and backup_match[0] is not None:
                        ycsm.delete(backup_match[0])
                else:
                    exit(0)

    else:
        logger.warning("No valid argument was given.")
        logger.warning(arguments)

    # s3_backup_objects = ycsm.list_available()
    # for key in s3_backup_objects:
    #     logger.info("Key : {}".format(key))


if __name__ == '__main__':
    main()
