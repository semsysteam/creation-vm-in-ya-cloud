#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import codecs
import json
import logging
import os
import sys

import chardet
import grpc
from six import with_metaclass
import yandexcloud
from yandex.cloud.compute.v1.image_service_pb2 import GetImageLatestByFamilyRequest
from yandex.cloud.compute.v1.image_service_pb2_grpc import ImageServiceStub
from yandex.cloud.compute.v1.instance_pb2 import IPV4, Instance, SchedulingPolicy
from yandex.cloud.compute.v1.instance_service_pb2 import (AttachedDiskSpec, CreateInstanceRequest, DeleteInstanceRequest, GetInstanceRequest, ListInstancesRequest, NetworkInterfaceSpec,
                                                          OneToOneNatSpec, PrimaryAddressSpec, ResourcesSpec, RestartInstanceMetadata, RestartInstanceRequest, StartInstanceMetadata,
                                                          StartInstanceRequest, StopInstanceMetadata, StopInstanceRequest)
from yandex.cloud.compute.v1.instance_service_pb2_grpc import InstanceServiceStub

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


def create_instance(sdk, folder_id, zone, name, subnet_id):
    image_service = sdk.client(ImageServiceStub)
    source_image = image_service.GetLatestByFamily(
        GetImageLatestByFamilyRequest(
            folder_id='standard-images',
            family='ubuntu-20.04'
        )
    )
    subnet_id = subnet_id or sdk.helpers.get_subnet(folder_id, zone)
    instance_service = sdk.client(InstanceServiceStub)
    logger.info('Creating initiated')
    return instance_service.Create(CreateInstanceRequest(
        folder_id=folder_id,
        name=name,
        resources_spec=ResourcesSpec(
            memory=8589934592,
            cores=4,
            core_fraction=50,
        ),
        zone_id=zone,
        platform_id='standard-v2',
        boot_disk_spec=AttachedDiskSpec(
            auto_delete=True,
            disk_spec=AttachedDiskSpec.DiskSpec(
                type_id='network-hdd',
                size=10 * 2 ** 30,
                image_id=source_image.id,
            )
        ),
        network_interface_specs=[
            NetworkInterfaceSpec(
                subnet_id=subnet_id,
                primary_v4_address_spec=PrimaryAddressSpec(
                    one_to_one_nat_spec=OneToOneNatSpec(
                        ip_version=IPV4,
                    )
                )
            ),
        ],
        scheduling_policy=SchedulingPolicy(
            preemptible=True,
        ),
    ))


def list_instances(sdk, folder_id, page_size, instance_name):
    logger.info('Listing instances from folder {}'.format(folder_id))
    instance_service = sdk.client(InstanceServiceStub)
    return instance_service.List(
        ListInstancesRequest(folder_id=folder_id, page_size=page_size, filter='name={}'.format(instance_name)))


def delete_instance(sdk, instance_id):
    logger.info('delete instance {}'.format(instance_id))
    instance_service = sdk.client(InstanceServiceStub)
    return instance_service.Delete(
        DeleteInstanceRequest(instance_id=instance_id))


def get_instance(sdk, instance_id):
    logger.info('Get info instance {}'.format(instance_id))
    instance_service = sdk.client(InstanceServiceStub)
    return instance_service.Get(
        GetInstanceRequest(instance_id=instance_id, view='FULL'))


def start_instance(sdk, instance_id):
    logger.info('Start instance {}'.format(instance_id))
    instance_service = sdk.client(InstanceServiceStub)
    return instance_service.Start(
        StartInstanceRequest(instance_id=instance_id))


def restart_instance(sdk, instance_id):
    logger.info('Restart instance {}'.format(instance_id))
    instance_service = sdk.client(InstanceServiceStub)
    return instance_service.Restart(
        RestartInstanceRequest(instance_id=instance_id))


def stop_instance(sdk, instance_id):
    logger.info('Stop instance {}'.format(instance_id))
    instance_service = sdk.client(InstanceServiceStub)
    return instance_service.Stop(
        StopInstanceRequest(instance_id=instance_id))


def get_internal_ipv4(instance, subnet_id):
    if instance.network_interfaces:
        for network_interface in instance.network_interfaces:
            if network_interface.subnet_id == subnet_id:
                if network_interface.primary_v4_address.address:
                    return network_interface.primary_v4_address.address
                else:
                    raise Exception(
                        'Instance with name: {0}, internal ipv4 address not found'.format(instance.name))
            else:
                raise Exception(
                    'Instance with name: {0}, in subnet with id: {1} not found'.format(instance.name, subnet_id))
    else:
        raise Exception(
            'Instance with name: {0}, network_interfaces not found'.format(instance.name))


def get_public_ip(instance, subnet_id):
    if instance.network_interfaces:
        for network_interface in instance.network_interfaces:
            if network_interface.subnet_id == subnet_id:
                if network_interface.primary_v4_address.one_to_one_nat:
                    one_to_one_nat = network_interface.primary_v4_address.one_to_one_nat
                    return one_to_one_nat.address
                else:
                    raise Exception(
                        'Instance with name: {0}, public ipv4 address not found'.format(instance.name))
            else:
                raise Exception(
                    'Instance with name: {0}, in subnet with id: {1} not found'.format(instance.name, subnet_id))
    else:
        raise Exception(
            'Instance with name: {0}, network_interfaces not found'.format(instance.name))


def fill_missing_arguments(sdk, arguments):
    if not arguments.subnet_id:
        network_id = sdk.helpers.find_network_id(folder_id=arguments.folder_id)
        arguments.subnet_id = sdk.helpers.find_subnet_id(
            folder_id=arguments.folder_id,
            zone_id=arguments.zone,
            network_id=network_id,
        )


def parse_args():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawTextHelpFormatter)
    auth = parser.add_mutually_exclusive_group(required=True)
    auth.add_argument('--token',
                      help='OAuth token')
    parser.add_argument('--instance_id',
                        help='Your Yandex.Cloud instance id',
                        required=True)
    parser.add_argument('--folder_id',
                        help='Your Yandex.Cloud folder id',
                        required=True)
    parser.add_argument('--subnet_id',
                        help='Your Yandex.Cloud subnet id',
                        required=True)
    parser.add_argument("--action",
                        metavar="action",
                        choices=['start', 'stop',
                                 'restart'],
                        help='If this option is used, the following values are possible: "start" or "stop" or "restart"',
                        required=False)
    user_args = parser.parse_args()
    return user_args


def set_ip_env(instance, subnet_id):
    public_ip_v4 = get_public_ip(
        instance, subnet_id)

    internal_ip_v4 = get_internal_ipv4(
        instance, subnet_id)

    if internal_ip_v4:
        os.environ['YC_INSTANCE_INTERNAL_IP'] = internal_ip_v4
    if public_ip_v4:
        os.environ['YC_INSTANCE_PUBLIC_IP'] = public_ip_v4
    print(os.environ['YC_INSTANCE_PUBLIC_IP'])

    return public_ip_v4


def main():
    ip_v4_address = None
    sdk = None
    arguments = parse_args()
    interceptor = yandexcloud.RetryInterceptor(
        max_retry_count=5, retriable_codes=[grpc.StatusCode.UNAVAILABLE])

    if arguments.token:
        sdk = yandexcloud.SDK(interceptor=interceptor,
                              token=arguments.token)
    else:
        filename = 'arguments.sa_json_path'
        with open(filename, 'rb') as rawfile:
            bts = min(32, os.path.getsize(filename))
            raw = rawfile.read(bts)
            if raw.startswith(codecs.BOM_UTF8):
                encoding = 'utf-8-sig'
            else:
                result = chardet.detect(raw)
                encoding = result['encoding']
            with open(file=filename, encoding=encoding) as infile:
                sdk = yandexcloud.SDK(interceptor=interceptor,
                                      service_account_key=json.load(infile))

    fill_missing_arguments(sdk, arguments)

    instance_id = arguments.instance_id
    instance = get_instance(sdk, instance_id=instance_id)

    if instance:
        instance_status = instance.status
        if arguments.action is None:
            # IF Instance Status STOPPED or STOPPING
            if instance_status != 2:
                operation = start_instance(
                    sdk, instance_id=instance_id)
                operation_result = sdk.wait_operation_and_get_result(
                    operation, response_type=Instance, meta_type=StartInstanceMetadata,)
                if operation_result or operation_result.response:
                    print(operation_result.response)
                    instance = get_instance(
                        sdk, instance_id=instance_id)
            ip_v4_address = set_ip_env(
                instance=instance, subnet_id=arguments.subnet_id)
        elif arguments.action == "start":
            # IF Instance Status is not RUNNING
            if instance_status != 2:
                operation = start_instance(sdk, instance_id=instance_id)
                operation_result = sdk.wait_operation_and_get_result(
                    operation, response_type=Instance, meta_type=StartInstanceMetadata,)
                if operation_result or operation_result.response:
                    print(operation_result.response)
                    instance = get_instance(
                        sdk, instance_id=instance_id)
            ip_v4_address = set_ip_env(
                instance=instance, subnet_id=arguments.subnet_id)
        elif arguments.action == "restart":
            # IF Instance Status RUNNING
            if instance_status == 2:
                operation = restart_instance(sdk, instance_id=instance_id)
                operation_result = sdk.wait_operation_and_get_result(
                    operation, response_type=Instance, meta_type=RestartInstanceMetadata,)
                if operation_result or operation_result.response:
                    print(operation_result.response)
                    instance = get_instance(
                        sdk, instance_id=instance_id)
            ip_v4_address = set_ip_env(
                instance=instance, subnet_id=arguments.subnet_id)
        elif arguments.action == "stop":
            # IF Instance Status RUNNING
            if instance_status == 2:
                operation = stop_instance(sdk, instance_id=instance_id)
                sdk.wait_operation_and_get_result(
                    operation, response_type=Instance, meta_type=StopInstanceMetadata,)
        elif arguments.action == "delete":
            # IF Instance Status STOPPED
            if instance_status != 4:
                o1 = stop_instance(sdk, instance_id=instance_id)
                sdk.wait_operation_and_get_result(
                    o1, response_type=Instance, meta_type=StopInstanceMetadata,)
                operation = delete_instance(sdk, instance_id=instance_id)
                sdk.wait_operation_and_get_result(
                    operation, response_type=Instance, meta_type=DeleteInstanceRequest,)
        else:
            raise Exception('Action not allowed')
    else:
        raise Exception(
            'Instance {} not found'.format(instance_id))

    return ip_v4_address


if __name__ == '__main__':
    main()
