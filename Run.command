#!/usr/bin/env bash
# /*
# * Created Date: Wednesday, July 7th 2021, 2:50:43 pm
# * Author: Sergey A. Kolesnikov
# *
# * Copyright (c) 2021
# */

set -euo pipefail

target="run.py"

dir=$(pwd)

if [[ $EUID = 0 ]]; then
    printf "ВНИМАНИЕ! Не запускайте этот скрипт пользователем root"
    exit 1
fi

if [ -z "$*" ]; then
    echo "No options found!"
    exit 1
fi

while getopts ":t:i:f:s:" flag; do
    case "$flag" in
    t) YC_OAUTH_TOKEN=${OPTARG}
       ;;
    i) YC_INSTANCE_ID=${OPTARG}
       ;;
    f) YC_FOLDER_ID=${OPTARG}
       ;;
    s) YC_SUBNET_ID=${OPTARG}
       ;;
    :)
       echo "Option -$OPTARG requires an argument (getopts)" >&2
       exit 1
       ;;
    *) echo "No reasonable options found!"
       ;;
    esac
done

command_exists() {
    command -v "$@" >/dev/null 2>&1
}
print_target_missing() {
    clear
    echo "  ###                      ###"
    echo " #      Файл Не Найден    #"
    echo "###                      ###"
    echo
    echo "Файл ${target} не найден!"
    echo
    exit 1
}
_main() {
    clear
    if [ ! -f "$dir/$target" ]; then
        print_target_missing
    fi
    if ! command_exists python3; then
        sudo apt-get update -qq >/dev/null
        sudo apt-get -yqq install python3
    fi
    if ! command_exists pip3; then
        sudo apt-get update -qq >/dev/null
        sudo apt-get -yqq install python3-pip
    fi

    pip3 install -r requirements.txt >/dev/null    

    python3 "$dir/$target" --token "$YC_OAUTH_TOKEN" --instance_id "$YC_INSTANCE_ID" --folder_id "$YC_FOLDER_ID" --subnet_id "$YC_SUBNET_ID"    
}

_main
